set :domain, '192.155.91.147'
set :deploy_to, "/home/caligram/#{APP_NAME}.staging.caligram.com"
set :repository, "git@gitlab.com:territoires/calendriers/caligram-#{APP_NAME}.git"
set :branch, 'master'
