# Caligram Base

Base calendar for Caligram.

## Creating a new calendar

1. Create a new project on GitLab called `caligram-PROJECT_NAME` under the `calendriers` group, and clone the repo locally. `PROJECT_NAME` should be lowercase and should be reused in subsequent steps below.
2. Add `caligram-base` as a git remote, and merge into `master`:

    ```
    git remote add base git@gitlab.com:territoires/calendriers/caligram-base && git merge base/master
    ```

3. In `config/deploy.rb`, change the value of `APP_NAME` to the `PROJECT_NAME` chosen earlier.
4. Create environment file. You must edit it immediately. Please refer to [this document](https://drive.google.com/file/d/0B0AvDaBwq1F0UmN5YnRYQm1NZlk/view) in order to know how to find the required values.

    ```
    cp .env.example .env
    ```
5. Create configuration files. You can edit them immediately or later, as required.

    ```
    cp src/config.js.example src/config.js && cp src/caligramReact.js.example src/caligramReact.js
    ```

6. Install npm dependencies, and start the development server:

    ```
    npm install
    npm run dev
    ```

## Customization

Some common ways in which calendars are typically customized:

* Edit `src/stylesheets/_variables.scss` to change [`caligram-react`](https://gitlab.com/territoires/caligram-react) SASS variables, including colors, font sizes, etc.
* Edit `src/components/Header.js` and `src/components/Footer.js` to implement custom headers and footers.
* If you need to use the header and footer from an existing site, you can scrape them using the `scrape` script.
    1. Copy `scrapeConfig.example.js` to `scrapeConfig.js`.
    2. Edit `scrapeConfig.js` to set the target website and specify the HTML you want to import for each component.
    3. Run `npm run scrape`. This will create React components, import stylesheets, and list scripts from the target website.
* Edit `src/config.js` to customize:
    * The calendar name, description, URL, and other metadata;
    * Whether the recurrence toggle, featured events, and other interface elements should be shown (`app.show`);
    * Which filters should be shown (`app.entities`), and in what order (`app.filtersEntitiesOrder`);
    * Third-party API keys and app IDs (Google Maps, Google Analytics);
    * External scripts that will be appended to `<body>` (`app.externalScripts`).
* Edit `src/caligramReact.js` to add or replace icons or localized strings used by [`caligram-react`](https://gitlab.com/territoires/caligram-react).
* In general, it’s okay to modify components files in `src/components`, but not in `src/containers`. Containers mostly manage the Redux state. Components generate markup.

## Updating

When a calendar has fallen behind the latest `caligram-react`, rebase with:

```
git fetch base && git rebase base/master
```

You may need to manage merge conflicts from time to time. ¯\\\_(ツ)\_/¯
