export default {
  event: {
    entities: {
      audiences: 'Publics',
      events: 'Événements',
      regions: 'Quartiers',
      types: 'Types',
      tags: 'Mots-clés',
      venues: 'Lieux',
      organizations: 'Organisations',
    },
    entitiesAll: {
      audiences: 'Tous les publics',
      events: 'Tous les événements',
      regions: 'Tous les quartiers',
      types: 'Tous les types',
      tags: 'Tous les mots-clés',
      venues: 'Tous les lieux',
      organizations: 'Toutes les organisations',
    },
    hideRepeatingEvents: 'Masquer les répétitions',
    recurringEvents: 'Événements récurrents',
    showAll: 'Afficher les %{entity} sans événements',
    treeView: 'Afficher la hiérarchie',
    collapseEntity: 'Masquer les sous-éléments',
    expandEntity: 'Afficher les sous-éléments',
  },
};
