module.exports = {
  events: {
    startsAt: 'Débute à',
    today: 'Aujourd\'hui',
  },
  filters: {
    audiences: {
      title: 'Public',
    },
    types: {
      title: 'Types d\'évènement',
    },
    regions: {
      municipalities: 'Municipalités',
      boroughs: 'Arrondissements',
      neighborhoods: 'Quartiers',
      places: 'Lieux',
    },
    prices: {
      title: 'Prix',
      free: 'Gratuit seulement',
      donationAndFree: 'Gratuit ou contribution volontaire',
    },
    tags: {
      title: 'Mots-clés',
      more: 'Afficher les %{count} thèmes...',
    },
  },
};
