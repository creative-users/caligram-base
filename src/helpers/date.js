import moment from 'moment';

export function closestHour(date) {
  return moment(date).startOf('hour');
}
