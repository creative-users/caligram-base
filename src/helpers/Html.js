import React, {Component, PropTypes} from 'react';
import BodyClassName from 'react-body-classname';
import ReactDOM from 'react-dom/server';
import serialize from 'serialize-javascript';
import DocumentMeta from 'react-document-meta';
import { app } from './../config';

/**
 * Wrapper component containing HTML metadata and boilerplate tags.
 * Used in server-side code only to wrap the string output of the
 * rendered route component.
 *
 * The only thing this component doesn't (and can't) include is the
 * HTML doctype declaration, which is added to the rendered output
 * by the server.js file.
 */
export default class Html extends Component {
  static propTypes = {
    assets: PropTypes.object,
    component: PropTypes.node,
    store: PropTypes.object,
  }

  render() {
    const {assets, component, store} = this.props;
    const content = component ? ReactDOM.renderToString(component) : '';
    const bodyClass = BodyClassName.rewind() || '';

    return (
      <html lang="en-us">
        <head>
          { !!app.googleTagManager && !!app.googleTagManager.id &&
            <script dangerouslySetInnerHTML={{__html: `
              (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
              new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
              j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
              'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
              })(window,document,'script','dataLayer','${app.googleTagManager.id}');
            `}}/>
          }
          {DocumentMeta.renderAsReact()}
          <link rel="shortcut icon" href="/favicon.ico" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          {/* styles (will be present only in production with webpack extract text plugin) */}
          {Object.keys(assets.styles).map((style, key) =>
            <link href={assets.styles[style]} key={key} rel="stylesheet" type="text/css" charSet="UTF-8"/>
          )}
          <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCoqafsgkxhVHfJzd98ukP11rEVEiLjuyE&v=3.20&libraries=geometry,drawing" />
          { !!app.googleAnalytics && !!app.googleAnalytics.id &&
            <script dangerouslySetInnerHTML={{__html: `
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
              ga('create', '${app.googleAnalytics.id}', 'auto');
              ga('send', 'pageview');
            `}}/>
            }
        </head>
        <body className={bodyClass}>
          { !!app.googleTagManager && !!app.googleTagManager.id &&
            <script dangerouslySetInnerHTML={{__html: `
              <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=${app.googleTagManager.id}"
              height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            `}}/>
          }
          <div id="content" dangerouslySetInnerHTML={{__html: content}}/>
          <script dangerouslySetInnerHTML={{__html: `window.__data=${serialize(store.getState())};`}} charSet="UTF-8"/>
          <script src={assets.javascript.main} charSet="UTF-8"/>
          { Array.isArray(app.externalScripts) &&
              app.externalScripts.map((script) => <script src={script} />) }
        </body>
      </html>
    );
  }
}
