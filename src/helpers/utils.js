export function capitalize(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

export function charRange(start, stop) {
  const result = [];
  for (let idx = start.charCodeAt(0), end = stop.charCodeAt(0); idx <= end; ++idx) {
    result.push(String.fromCharCode(idx));
  }
  return result;
}

export function outerWidth(el) {
  let width = el.offsetWidth;
  const style = getComputedStyle(el);

  width += parseInt(style.marginLeft, 10) + parseInt(style.marginRight, 10);
  return width;
}

export function filterCollectionFromParams(source, params, entity) {
  if (!params) {
    return [];
  }

  const filteredSource = source.filter((item) => {
    return params.indexOf(item.id) > -1 || params.indexOf(`${item.id}-${item.group}`) > -1;
  });

  if (entity && filteredSource.length > 1) {
    return filteredSource.filter(item => item.group === entity);
  }

  return filteredSource;
}

export function updateSelections(item, selected, select) {
  const query = {
    page: 1,
  };

  Object.keys(selected).forEach((key) => {
    const ids = selected[key].map((sel) => {
      return sel.id;
    });

    if (ids.length > 0) {
      query[key] = ids.join(',');
    }
  });

  if (!item || !Array.isArray(selected[item.group]) || typeof select[item.group] !== 'function') {
    return query;
  }

  const index = selected[item.group].indexOf(item);
  const ids = selected[item.group].map((se) => { return se.id; });
  switch (index > -1) {
    case true: // Present
      ids.splice(index, 1);

      if (ids.length === 0) {
        delete query[item.group];
      } else {
        query[item.group] = ids.join(',');
      }

      select[item.group](ids);
      break;
    case false: // Absent
    default:
      ids.push(item.id);
      query[item.group] = ids.join(',');
      select[item.group](ids);
  }

  return query;
}

export function splitIdsList(list) {
  if (!list) {
    return [];
  }

  return list.split(',').map((str) => {
    return parseInt(str.trim(), 10);
  });
}

export function findInCollectionOrAdd(item, collection) {
  if (!collection.find((seenItem) => {
    return seenItem.id === item.id;
  })) {
    collection.push(item);
  }
  return collection;
}

export function mergeCollections(items, collection) {
  for (const item of items) {
    findInCollectionOrAdd(item, collection);
  }

  return collection;
}

export function orderByField(collection, field) {
  if (!Array.isArray(collection)) {
    return collection;
  }

  return collection.sort((itemA, itemB) => {
    return String(itemA[field]).localeCompare(String(itemB[field]), 'fr');
  });
}

export function parseRange(rangeParam) {
  if (!rangeParam || rangeParam.indexOf(',') === -1) {
    return { start: null, end: null };
  }

  const rangeParts = rangeParam.split(',');
  return {
    start: rangeParts[0],
    end: rangeParts[1],
  };
}
