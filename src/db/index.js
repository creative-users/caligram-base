import path from 'path';
import DB from 'super-simple-db';

export default new DB(path.join(__dirname, 'blob.json'));
