import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { Button, Checkbox, Page } from 'caligram-react';
import { app } from '../config';

export default function Entities(props, { polyglot }) {
  const {
    disclosed,
    entities,
    entitiesTree,
    entity,
    handleToggleDisclosure,
    handleToggleShowAll,
    handleToggleTreeView,
    push,
    showAll,
    treeView,
  } = props;

  const backButtonAction = function() {
    push({ pathname: '/' });
  };

  const buildEntityLink = function(entityName, en) {
    return (
      <span className="entity">
        <Link to={{ pathname: '/evenements', query: { page: 1, [entityName]: en.id } }}>
          {en.name}
        </Link>
        <span className="references">{en._references !== undefined ? en._references : ''}</span>
      </span>
    );
  };

  const Item = function({ item }) {
    const liProps = { key: item.id };
    const hasChildren = item.children && item.children.length > 0;
    let isDisclosed = true;

    if (treeView && hasChildren) {
      if (app.show.disclosableEntities) {
        isDisclosed = disclosed.indexOf(item.id) > -1;
        liProps.className = `parent ${app.show.disclosableEntities && isDisclosed ? 'expanded' : 'collapsed'}`;
      } else {
        liProps.className = 'parent';
      }
    }

    const title = isDisclosed
      ? polyglot.t('event.collapseEntity')
      : polyglot.t('event.expandEntity');
    const icon = isDisclosed ? 'chevronUp' : 'chevronDown';

    return (
      <li {...liProps}>
        {buildEntityLink(entity, item)}
        {app.show.disclosableEntities && hasChildren && treeView &&
            <Button title={title} action={() => handleToggleDisclosure(item.id)} hideTitle iconLeft={icon} />
            }

            {(treeView && hasChildren && isDisclosed) ? (
              <ul className="children">
              {item.children.map((cEn) => {
                return (<Item key={`${cEn.id}-${cEn.parent_id}`} item={cEn} />);
              })}
              </ul>
              ) : ''}
      </li>
    );
  };

  return (
    <Page title={polyglot.t(`event.entitiesAll.${entity}`)} backButtonAction={backButtonAction} layout="wide" className="Entities">
      {app.show.entityFilters &&
        <ul className="filters">
          <li><Checkbox title={polyglot.t('event.showAll', { entity })} action={handleToggleShowAll} checked={showAll} id="toggleShowAll" /></li>
          <li><Checkbox title={polyglot.t('event.treeView')} action={handleToggleTreeView} checked={treeView} id="toggleTreeView" /></li>
        </ul>
      }
      <ul className={`entities ${treeView ? 'tree' : 'flat'}`}>
        {(treeView ? entitiesTree : entities).map((en) => {
          return (<Item key={`${en.id}-${en.parent_id}`} item={en} />);
        })}
      </ul>
    </Page>
  );
}

Entities.contextTypes = {
  polyglot: PropTypes.object,
};

Entities.propTypes = {
  disclosed: PropTypes.array,
  entities: PropTypes.array,
  entitiesTree: PropTypes.array,
  entity: PropTypes.string,
  handleToggleDisclosure: PropTypes.func,
  handleToggleShowAll: PropTypes.func,
  handleToggleTreeView: PropTypes.func,
  push: PropTypes.func,
  showAll: PropTypes.bool,
  treeView: PropTypes.bool,
};
