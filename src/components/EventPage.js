import React, { PropTypes } from 'react';
import DocumentMeta from 'react-document-meta';
import { EventDetail, EventMeta } from 'caligram-react';
import config from '../config.js';

export default function EventPage({ event, location, push }) {
  const actions = {
    backButtonAction: () => push({ pathname: '/' }),
    audienceAction: audience => push({ pathname: '/evenements', query: { audiences: audience.id } }),
    typeAction: type => push({ pathname: '/evenements', query: { types: type.id } }),
    tagAction: tag => push({ pathname: '/evenements', query: { tags: tag.id } }),
    groupAction: group => push({ pathname: '/evenements', query: { organizations: group.id } }),
  };

  return (
    <div className="EventPage">
      <EventMeta component={DocumentMeta} event={event} siteName={config.app.title} twitterUsername={config.app.meta.property['twitter:creator']} />
      <EventDetail event={event} mapApiKey={config.app.googleMaps.apiKey} selectedDate={location.query.date} {...actions} />
    </div>
  );
}

EventPage.propTypes = {
  event: PropTypes.object.isRequired,
  location: PropTypes.any,
  params: PropTypes.object.isRequired,
};
