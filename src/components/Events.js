import React from 'react';
import { EventBrowserContainer } from 'containers';

export default function Events() {
  return (
    <div className="Events container">
      <EventBrowserContainer title="Résultats de recherche" />
    </div>
  );
}
