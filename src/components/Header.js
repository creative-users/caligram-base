import React from 'react';
import config from '../config';
import { IndexLink } from 'react-router';
import { Menu } from '../components';

export default function Header() {
  return (
    <header className="Header">
      <div className="container">
        <IndexLink to="/" className="logo"><img src="/logo-caligram.svg" alt={config.app.title}/></IndexLink>
        <Menu />
      </div>
    </header>
  );
}
