import React from 'react';
import { Header, Footer } from 'components';

export default function App({ children, setPageTitle }) {
  return (
    <div className="App">
      <Header/>
      {React.cloneElement(children, { setPageTitle: setPageTitle })}
      <Footer />
    </div>
  );
}
