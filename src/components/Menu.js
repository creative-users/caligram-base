import React from 'react';
import { IndexLink, Link } from 'react-router';

export default function Menu() {
  return (
    <nav className="Menu">
      <ul>
        <li><IndexLink to="/" activeClassName="current">Accueil</IndexLink></li>
        <li><Link to="/a-propos" activeClassName="current">À propos</Link></li>
        <li><Link to="/types" activeClassName="current">Par type</Link></li>
        <li><Link to="/themes" activeClassName="current">Par thème</Link></li>
        <li><Link to="/lieux" activeClassName="current">Par lieu</Link></li>
        <li><Link to="/groupes" activeClassName="current">Par groupe</Link></li>
      </ul>
    </nav>
  );
}
