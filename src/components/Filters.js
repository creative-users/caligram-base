import React, { PropTypes } from 'react';
import { HeadingBlock, SelectionList } from 'caligram-react';
import config from '../config';
import { CalendarContainer, RecurrencesButtonContainer, SearchBarContainer } from 'containers';
import { capitalize } from 'helpers/utils';

export default function Filters(props, { polyglot }) {
  const { action, clear } = props;

  return (
    <nav id="filters" className="Filters">
      <SearchBarContainer />
      <CalendarContainer />
      { config.app.show.recurrencesButton &&
        <HeadingBlock title={polyglot.t('event.recurringEvents')}>
          <RecurrencesButtonContainer />
        </HeadingBlock>
      }
      { config.app.filtersEntitiesOrder.map(entity => {
        if (config.app.entities[entity].inFilters) {
          return (
            <HeadingBlock key={`filters${capitalize(entity)}`} className={entity} title={polyglot.t(`event.entities.${entity}`)}>
              <SelectionList available={props[entity]} selected={props[`selected${capitalize(entity)}`]} groupKey="none" clearText={polyglot.t(`event.entitiesAll.${entity}`)} action={action} clear={clear.bind(this, [entity])} labelKey="name" />
            </HeadingBlock>
          );
        }
      }) }
    </nav>
  );
}

Filters.contextTypes = {
  polyglot: PropTypes.object,
};
