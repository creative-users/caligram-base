import React from 'react';
import config from '../config';
import { EventBrowserContainer, ShowcaseContainer } from 'containers';

export default function Home() {
  return (
    <div className="Home">
      { !!config.app.showcases && config.app.showcases.featured &&
        <aside>
          <ShowcaseContainer query={config.app.showcases.featured} />
        </aside>
      }
      <EventBrowserContainer title="Tous les événements" />
    </div>
  );
}
