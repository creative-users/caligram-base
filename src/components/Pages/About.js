import React from 'react';
import { Page } from 'caligram-react';
import DocumentMeta from 'react-document-meta';

import config from '../../config';

export default function About() {
  return (
    <div>
      <DocumentMeta {...config.app} />
      <Page title="À propos">
        <p>Caligram est un calendrier incroyable.</p>
      </Page>
    </div>
  );
}
