const fs = require('fs');
const got = require('got');
const cheerio = require('cheerio');
const HTMLtoJSX = require('htmltojsx');
const chalk = require('chalk');
const path = require('path');
const config = require('./scrapeConfig');

function wrapComponent(name, string) {
  const indentedString = string.replace(/^(?!\s*$)/mg, '    ');
  return `
import React from 'react';

export default function ${name}() {
  return (
${indentedString}  );
}
`.trimLeft();
}

function writeFile(filePath, content, successMessage) {
  fs.writeFile(filePath, content, (error) => {
    if (error) {
      return console.error(chalk.red(error));
    }
    console.log(chalk.green(successMessage));
    console.log(chalk.gray(filePath), '\n');
  });
}

(async () => {
  try {
    // Fetch HTML from remote source
    const response = await got(config.url);
    const $ = cheerio.load(response.body);

    // Import components as specified in scrapeConfig.js
    config.components.forEach((component) => {
      const element = $(component.selector);
      const html = component.transformer ? $.html(component.transformer(element)) : $.html(component.selector);
      const converter = new HTMLtoJSX({
        createClass: false,
        outputClassName: component.name,
      });
      const output = converter.convert(html);
      const filePath = path.join(__dirname, 'components', `${component.name}.js`);
      let wrappedOutput = wrapComponent(component.name, output);
      wrappedOutput = wrappedOutput.replace(/> +</g, '> <'); // Sometimes HTMLtoJSX leaves multiple spaces between tags
      wrappedOutput = wrappedOutput.replace(/xlink="/g, 'xmlnsXlink="'); // Kludgy
      wrappedOutput = wrappedOutput.replace(/space="/g, 'xmlSpace="'); // Kludgy
      writeFile(filePath, wrappedOutput, `Created component ${component.name}`);
    });

    // Import stylesheets
    const stylesheets = $('link[rel=stylesheet]');
    const styleTags = $('style');
    if (stylesheets.length > 0 || styleTags.length > 0) {
      let cssFileContent = '// This file was generated automatically by `npm run scrape`\n';
      stylesheets.each(function() {
        cssFileContent += `@import url(${$(this).attr('href')});\n`;
      });
      styleTags.each(function() {
        cssFileContent += $(this).html();
      });
      const cssFilePath = path.join(__dirname, 'stylesheets', '_scraped.scss');
      writeFile(cssFilePath, cssFileContent, `Scraped ${stylesheets.length + styleTags.length} stylesheets to _scraped.scss`);
    }

    // Find external scripts
    const externalScripts = $('script[src]');
    if (externalScripts.length > 0) {
      console.log(chalk.green(`Found ${externalScripts.length} external scripts`));
      console.log(chalk.yellow('External scripts are not added automatically. You may add them manually to `config.js` under `externalScripts`.'));
      externalScripts.each(function() {
        console.log(chalk.gray($(this).attr('src')));
      });
      console.log(); // We just want a newline
    }

    // Find inline scripts
    const inlineScripts = $('script:not([src])');
    if (inlineScripts.length > 0) {
      console.log(chalk.green(`Found ${inlineScripts.length} inline scripts`));
      console.log(chalk.yellow('Inline scripts are not added automatically. Here they are, sorry for the noise.'));
      inlineScripts.each(function() {
        console.log(chalk.gray($(this).html()));
      });
      console.log(); // We just want a newline
    }
  } catch (error) {
    console.error(chalk.red(`Error scraping ${config.url}`));
    console.log(error);
  }
})();
