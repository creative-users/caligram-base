import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { push } from 'redux-router';
import { EventList } from 'caligram-react';
import { setSelected as setSelectedAudiences } from 'redux/modules/audiences';
import { setSelected as setSelectedOrganizations } from 'redux/modules/organizations';
import { setSelected as setSelectedRegions } from 'redux/modules/regions';
import { setSelected as setSelectedTags } from 'redux/modules/tags';
import { setSelected as setSelectedTypes } from 'redux/modules/types';
import { setSelected as setSelectedVenues } from 'redux/modules/venues';
import { updateSelections } from 'helpers/utils';

@connect(state => ({
  events: state.events.data,
  loading: state.events.loading,
  location: state.router.location,
  pagination: state.events.pagination,
  selectedAudiences: state.audiences.selected,
  selectedOrganizations: state.organizations.selected,
  selectedRegions: state.regions.selected,
  selectedTags: state.tags.selected,
  selectedTypes: state.types.selected,
  selectedVenues: state.venues.selected,
}), {
  push,
  setSelectedAudiences,
  setSelectedOrganizations,
  setSelectedRegions,
  setSelectedTags,
  setSelectedTypes,
  setSelectedVenues,
})
export default class EventListContainer extends Component {
  static propTypes = {
    children: PropTypes.func,
    events: PropTypes.array,
    loading: PropTypes.bool,
    location: PropTypes.object,
    pagination: PropTypes.object,
    push: PropTypes.func.isRequired,
    selectedAudiences: PropTypes.array.isRequired,
    selectedOrganizations: PropTypes.array.isRequired,
    selectedRegions: PropTypes.array.isRequired,
    selectedTags: PropTypes.array.isRequired,
    selectedTypes: PropTypes.array.isRequired,
    selectedVenues: PropTypes.array.isRequired,
    setSelectedAudiences: PropTypes.func.isRequired,
    setSelectedOrganizations: PropTypes.func.isRequired,
    setSelectedRegions: PropTypes.func.isRequired,
    setSelectedTags: PropTypes.func.isRequired,
    setSelectedTypes: PropTypes.func.isRequired,
    setSelectedVenues: PropTypes.func.isRequired,
    title: PropTypes.string,
  };

  static contextTypes = {
    moment: PropTypes.func,
    router: PropTypes.object,
  };

  static defaultProps = {
    children: (props) => { return <EventList {...props} />; },
  };

  handlePageChange(page) {
    const { location, push } = this.props; // eslint-disable-line no-shadow
    const query = {
      ...location.query,
      page: page,
    };

    push({ pathname: '/evenements', query: query });
  }

  handleRemoveItem(item) {
    const { location, push } = this.props; // eslint-disable-line no-shadow
    const selected = {
      audiences: this.props.selectedAudiences,
      organizations: this.props.selectedOrganizations,
      regions: this.props.selectedRegions,
      types: this.props.selectedTypes,
      tags: this.props.selectedTags,
      venues: this.props.selectedVenues,
    };
    const select = {
      audiences: this.props.setSelectedAudiences,
      organizations: this.props.setSelectedOrganizations,
      regions: this.props.setSelectedRegions,
      types: this.props.setSelectedTypes,
      tags: this.props.setSelectedTags,
      venues: this.props.setSelectedVenues,
    };
    let newQuery = { ...location.query };

    if (item.group === 'range') {
      delete newQuery.range;
    } else {
      newQuery = updateSelections(item, selected, select);
    }

    push({ pathname: '/evenements', query: newQuery });
  }

  handleClear() {
    [
      this.props.setSelectedAudiences,
      this.props.setSelectedOrganizations,
      this.props.setSelectedRegions,
      this.props.setSelectedTypes,
      this.props.setSelectedTags,
      this.props.setSelectedVenues,
    ].forEach((method) => method([]));

    this.props.push({ pathname: '/evenements' });
  }

  render() {
    const { events, loading, location, pagination, title } = this.props;
    const {
      selectedAudiences,
      selectedOrganizations,
      selectedRegions,
      selectedTypes,
      selectedTags,
      selectedVenues,
    } = this.props;
    const { moment } = this.context;

    const range = [];
    if (location.query.range) {
      const date = {
        id: location.query.range,
        label: moment(location.query.range.split(',')[0]).format('Do MMMM YYYY'),
        tag: 'Date',
        group: 'range',
      };
      range.push(date);
    }

    const selectedItems = range.concat(selectedAudiences)
      .concat(selectedOrganizations)
      .concat(selectedRegions)
      .concat(selectedTypes)
      .concat(selectedTags)
      .concat(selectedVenues);

    const properties = {
      clearFilters: ::this.handleClear,
      currentPage: pagination.current_page,
      events,
      filters: selectedItems,
      pageChange: ::this.handlePageChange,
      removeFilter: ::this.handleRemoveItem,
      totalPages: pagination.total_pages,
      loading,
      title,
    };

    return this.props.children(properties);
  }
}
