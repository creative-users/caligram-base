import React, { Component } from 'react';
import { index as eventIndex, selectRange } from 'redux/modules/events';
import { EventsMap } from '../components';
import connectData from 'helpers/connectData';
import { stringify } from 'querystring';
import moment from 'moment';

function fetchData(getState, dispatch, location) {
  const promises = [];
  const map = getState().events.map;

  if (location.query.range) {
    const range = location.query.range.split(',').map((time) => { return moment(time); });

    if (range.length) {
      const start = range[0] ? range[0].format('YYYY-MM-DD HH:mm:ss') : null;
      const end = range[1] ? range[1].format('YYYY-MM-DD HH:mm:ss') : null;
      promises.push(dispatch(selectRange({start: start, end: end})));
    }
  }

  const { centerLat, centerLng, radius } = map;
  const query = {
    distance: `${centerLat},${centerLng},${radius}`,
    order: 'start_date',
    recurrences: false,
  };

  // force reload events with back and next browser buttons
  promises.push(dispatch(eventIndex(
    location.search === ''
      ? '?' + stringify(query)
      : (location.search + stringify(query))

  )));

  return Promise.all(promises);
}

@connectData(fetchData)
export default class EventsMapContainer extends Component {
  render() {
    return <EventsMap />;
  }
}
