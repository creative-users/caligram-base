import React, { Component, PropTypes } from 'react';
import DocumentMeta from 'react-document-meta';
import { connect } from 'react-redux';
import { push } from 'redux-router';
import { Entities } from '../components';
import {
  index as indexOrganizations,
  setShowAll as setShowAllOrganizations,
  setTreeView as setTreeViewOrganizations,
  toggleDisclosed as toggleDisclosedOrganizations,
} from 'redux/modules/organizations';
import {
  index as indexTags,
  setShowAll as setShowAllTags,
  setTreeView as setTreeViewTags,
  toggleDisclosed as toggleDisclosedTags,
} from 'redux/modules/tags';
import {
  index as indexTypes,
  setShowAll as setShowAllTypes,
  setTreeView as setTreeViewTypes,
  toggleDisclosed as toggleDisclosedTypes,
} from 'redux/modules/types';
import {
  index as indexVenues,
  setShowAll as setShowAllVenues,
  setTreeView as setTreeViewVenues,
  toggleDisclosed as toggleDisclosedVenues,
} from 'redux/modules/venues';

import config from '../config';

const mapDispatchToProps = (dispatch) => {
  return {
    indexOrganizations: (showAll, treeView, query, flush) => {
      dispatch(indexOrganizations(showAll, treeView, query, flush));
    },
    setShowAllOrganizations: (value) => {
      dispatch(setShowAllOrganizations(value));
    },
    setTreeViewOrganizations: (value) => {
      dispatch(setTreeViewOrganizations(value));
    },
    toggleDisclosedOrganizations: (value) => {
      dispatch(toggleDisclosedOrganizations(value));
    },
    indexTags: (showAll, treeView, query, flush) => {
      dispatch(indexTags(showAll, treeView, query, flush));
    },
    setShowAllTags: (value) => {
      dispatch(setShowAllTags(value));
    },
    setTreeViewTags: (value) => {
      dispatch(setTreeViewTags(value));
    },
    toggleDisclosedTags: (value) => {
      dispatch(toggleDisclosedTags(value));
    },
    indexTypes: (showAll, treeView, query, flush) => {
      dispatch(indexTypes(showAll, treeView, query, flush));
    },
    setShowAllTypes: (value) => {
      dispatch(setShowAllTypes(value));
    },
    setTreeViewTypes: (value) => {
      dispatch(setTreeViewTypes(value));
    },
    toggleDisclosedTypes: (value) => {
      dispatch(toggleDisclosedTypes(value));
    },
    indexVenues: (showAll, treeView, query, flush) => {
      dispatch(indexVenues(showAll, treeView, query, flush));
    },
    setShowAllVenues: (value) => {
      dispatch(setShowAllVenues(value));
    },
    setTreeViewVenues: (value) => {
      dispatch(setTreeViewVenues(value));
    },
    toggleDisclosedVenues: (value) => {
      dispatch(toggleDisclosedVenues(value));
    },
    push: (value) => {
      dispatch(push(value));
    },
  };
};

@connect(state => ({
  organizations: {
    data: state.organizations.data,
    showAll: state.organizations.showAll,
    treeView: state.organizations.treeView,
    disclosed: state.organizations.disclosed,
  },
  tags: {
    data: state.tags.data,
    showAll: state.tags.showAll,
    treeView: state.tags.treeView,
    disclosed: state.tags.disclosed,
  },
  types: {
    data: state.types.data,
    showAll: state.types.showAll,
    treeView: state.types.treeView,
    disclosed: state.types.disclosed,
  },
  venues: {
    data: state.venues.data,
    showAll: state.venues.showAll,
    treeView: state.venues.treeView,
    disclosed: state.venues.disclosed,
  },
  location: state.router.location,
}), mapDispatchToProps)
export default class EntitiesContainer extends Component {
  static propTypes = {
    entity: PropTypes.string.isRequired,
    indexOrganizations: PropTypes.func.isRequired,
    indexTags: PropTypes.func.isRequired,
    indexTypes: PropTypes.func.isRequired,
    indexVenues: PropTypes.func.isRequired,
    organizations: PropTypes.object.isRequired,
    push: PropTypes.func.isRequired,
    setShowAllOrganizations: PropTypes.func.isRequired,
    setShowAllTags: PropTypes.func.isRequired,
    setShowAllTypes: PropTypes.func.isRequired,
    setShowAllVenues: PropTypes.func.isRequired,
    setTreeViewOrganizations: PropTypes.func.isRequired,
    setTreeViewTags: PropTypes.func.isRequired,
    setTreeViewTypes: PropTypes.func.isRequired,
    setTreeViewVenues: PropTypes.func.isRequired,
    toggleDisclosedOrganizations: PropTypes.func.isRequired,
    toggleDisclosedTags: PropTypes.func.isRequired,
    toggleDisclosedTypes: PropTypes.func.isRequired,
    toggleDisclosedVenues: PropTypes.func.isRequired,
    tags: PropTypes.object.isRequired,
    types: PropTypes.object.isRequired,
    venues: PropTypes.object.isRequired,
  };

  static contextTypes = {
    moment: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.actions = {
      organizations: {
        index: this.props.indexOrganizations,
        setShowAll: this.props.setShowAllOrganizations,
        setTreeView: this.props.setTreeViewOrganizations,
        toggleDisclosed: this.props.toggleDisclosedOrganizations,
      },
      tags: {
        index: this.props.indexTags,
        setShowAll: this.props.setShowAllTags,
        setTreeView: this.props.setTreeViewTags,
        toggleDisclosed: this.props.toggleDisclosedTags,
      },
      types: {
        index: this.props.indexTypes,
        setShowAll: this.props.setShowAllTypes,
        setTreeView: this.props.setTreeViewTypes,
        toggleDisclosed: this.props.toggleDisclosedTypes,
      },
      venues: {
        index: this.props.indexVenues,
        setShowAll: this.props.setShowAllVenues,
        setTreeView: this.props.setTreeViewVenues,
        toggleDisclosed: this.props.toggleDisclosedVenues,
      },
    };
  }

  handleToggleDisclosure(entityId) {
    const { entity } = this.props;
    this.actions[entity].toggleDisclosed(entityId);
  }

  handleToggleShowAll() {
    const { entity } = this.props;
    const newShowAll = !this.props[entity].showAll;
    const treeView = this.props[entity].treeView;

    this.actions[entity].setShowAll(newShowAll);
    this.actions[entity].index(newShowAll, treeView, {}, !newShowAll);
  }

  handleToggleTreeView() {
    const { entity } = this.props;
    const showAll = this.props[entity].showAll;
    const newTreeView = !this.props[entity].treeView;

    this.actions[entity].setTreeView(!this.props[entity].treeView);
    this.actions[entity].index(showAll, newTreeView, {}, showAll);
  }

  render() {
    const entity = this.props.entity;
    const entities = this.props[entity].data;
    const showAll = this.props[entity].showAll;
    const treeView = this.props[entity].treeView;

    const entitiesTree = [];
    const childrenByParentId = {};
    const sortByName = (eA, eB) => {
      return eA.name.localeCompare(eB.name);
    };

    const addReferences = function(acc, en) {
      if (Array.isArray(en.children) && en.references === undefined) {
        en._references = en.children.reduce(addReferences, 0);
      } else {
        en._references = en.references;
        en.children.reduce(addReferences, 0);
      }

      if (!isNaN(parseInt(en.references, 10))) {
        return acc + en.references;
      }

      return acc;
    };

    if (treeView) {
      let root;
      entities.forEach((en) => {
        if (en.parent_id) {
          childrenByParentId[en.parent_id] = childrenByParentId[en.parent_id] || [];
          childrenByParentId[en.parent_id].push(en);
        } else {
          root = en;
          root.children = [];
          entitiesTree.push(en);
        }
      });

      entities.forEach((en) => {
        childrenByParentId[en.id] = childrenByParentId[en.id] || [];
        en.children = childrenByParentId[en.id];
      });

      entitiesTree.sort(sortByName);
      for (const childId of Object.keys(childrenByParentId)) {
        childrenByParentId[childId].sort(sortByName);
      }

      entitiesTree.reduce(addReferences, 0);
    } else {
      for (const ent of entities) {
        ent._references = ent.references;
      }
    }

    const props = {
      disclosed: this.props[entity].disclosed,
      entity,
      entities,
      entitiesTree,
      push: this.props.push,
      showAll,
      treeView,
      handleToggleDisclosure: this.handleToggleDisclosure.bind(this),
      handleToggleShowAll: this.handleToggleShowAll.bind(this),
      handleToggleTreeView: this.handleToggleTreeView.bind(this),
    };

    return (
      <div>
        <DocumentMeta {...config.app} />
        <Entities {...props} />
      </div>
    );
  }
}
