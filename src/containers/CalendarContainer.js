import React, {Component, PropTypes} from 'react';
import { Calendar } from 'caligram-react';
import {connect} from 'react-redux';
import {push} from 'redux-router';
import {selectRange} from 'redux/modules/events';

@connect(state => ({
  location: state.router.location,
  selectedRange: state.events.selectedRange,
}), {push, selectRange})
export default class CalendarContainer extends Component {
  static propTypes = {
    location: PropTypes.object,
    push: PropTypes.func.isRequired,
    selectRange: PropTypes.func.isRequired,
    selectedRange: PropTypes.object.isRequired,
  };

  static contextTypes = {
    moment: PropTypes.func,
  };

  onSelect(moment) {
    const {push, location, selectRange, selectedRange} = this.props; // eslint-disable-line no-shadow
    const start = moment.clone().startOf('day');
    const end = start.clone().endOf('day');
    const isSameDay = moment.isSame(selectedRange.start, 'day');
    const query = {
      ...location.query,
      page: 1,
    };

    if (isSameDay) {
      // Remove range from query
      delete query.range;
      selectRange({start: null, end: null});
    } else {
      // Add/update range to query
      const format = 'YYYY-MM-DD HH:mm:ss';
      query.range = `${start.format(format)},${end.format(format)}`;
    }

    push({ pathname: '/evenements', query: query });

    return true; // Calendar flag...
  }

  render() {
    const { moment } = this.context;
    const { selectedRange } = this.props;
    let date = null;

    if (selectedRange && selectedRange.start) {
      date = moment(selectedRange.start);
    }

    return (
      <Calendar date={date} locale="fr" onSelect={::this.onSelect} />
    );
  }
}
