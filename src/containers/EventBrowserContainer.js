import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { push } from 'redux-router';
import { EventBrowser } from 'components';

@connect(state => ({
  mapView: state.events.mapView,
}), { push })
export default class EventBrowserContainer extends Component {
  static propTypes = {
    mapView: PropTypes.bool,
    title: PropTypes.string,
    push: PropTypes.func.isRequired,
  };

  render() {
    return <EventBrowser {...this.props} />;
  }
}
