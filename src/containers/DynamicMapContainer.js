import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { push } from 'redux-router';
import { default as haversine } from 'haversine';
import { GeolocatedDynamicMap, MapMarker } from 'caligram-react';
import { geoIndex, setMapCenter } from 'redux/modules/events';
import config from '../config';

const mapDispatchToProps = (dispatch) => {
  return {
    push,
    dispatchSetMapCenter: (newCenter) => {
      dispatch(setMapCenter(newCenter));
    },
    dispatchGeoIndex: (center, radius) => {
      dispatch(geoIndex(center, radius));
    },
  };
};

@connect(state => ({
  events: state.events.geoData,
  loading: state.events.loading,
  location: state.router.location,
  map: state.events.map,
}), mapDispatchToProps)
export default class DynamicMapContainer extends Component {
  static propTypes = {
    dispatchGeoIndex: PropTypes.func.isRequired,
    dispatchSetMapCenter: PropTypes.func.isRequired,
    events: PropTypes.array,
    loading: PropTypes.bool,
    location: PropTypes.object,
    map: PropTypes.object,
    push: PropTypes.func.isRequired,
  };

  static contextTypes = {
    moment: PropTypes.func,
    router: PropTypes.object,
  };

  render() {
    const { centerLat, centerLng, initialZoom } = this.props.map;
    const { dispatchGeoIndex, dispatchSetMapCenter } = this.props;
    const { events, loading, push } = this.props; // eslint-disable-line no-shadow

    const mapEvents = [];
    const eventsById = [];

    events.filter((ev) => {
      return ev.venue.latitude && ev.venue.longitude;
    }).map((ev) => {
      if (!eventsById[ev.id]) {
        ev.lat = ev.venue.latitude;
        ev.lng = ev.venue.longitude;

        mapEvents.push(ev);
        eventsById[ev.id] = ev;
      }
    });

    const properties = {
      apiKey: config.app.googleMaps.apiKey,
      centerLat,
      centerLng,
      events: mapEvents,
      initialZoom,
      className: { loading },
      handleOnClick: (id) => {
        const eventObj = eventsById[id];
        push({ pathname: `/evenements/${eventObj.slug}/${eventObj.id}` }, {
          date: eventObj.date,
        });
      },
      marker: MapMarker,
      onCenterChange: (mapData) => {
        const radius = haversine({
          latitude: mapData.center.lat,
          longitude: mapData.center.lng,
        }, {
          latitude: mapData.bounds.nw.lat,
          longitude: mapData.bounds.nw.lng,
        }, { unit: 'meter' });

        dispatchSetMapCenter(mapData);

        dispatchGeoIndex(mapData.center, radius);
      },
    };

    return (
      <GeolocatedDynamicMap mapProps={properties} />
    );
  }
}
