import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Filters } from '../components';
import { push } from 'redux-router';
import { updateSelections } from 'helpers/utils';
import { setSelected as setSelectedAudiences } from 'redux/modules/audiences';
import { setSelected as setSelectedOrganizations } from 'redux/modules/organizations';
import { setSelected as setSelectedRegions } from 'redux/modules/regions';
import { setSelected as setSelectedTags } from 'redux/modules/tags';
import { setSelected as setSelectedTypes } from 'redux/modules/types';
import { setSelected as setSelectedVenues } from 'redux/modules/venues';

@connect(
  state => ({
    audiences: state.audiences.menu,
    location: state.router.location,
    organizations: state.organizations.menu,
    regions: state.regions.menu,
    selectedAudiences: state.audiences.selected,
    selectedOrganizations: state.organizations.selected,
    selectedRegions: state.regions.selected,
    selectedTags: state.tags.selected,
    selectedTypes: state.types.selected,
    selectedVenues: state.venues.selected,
    tags: state.tags.menu,
    types: state.types.menu,
    venues: state.venues.menu,
  }),
  {
    push,
    setSelectedAudiences,
    setSelectedOrganizations,
    setSelectedRegions,
    setSelectedTags,
    setSelectedTypes,
    setSelectedVenues,
  }
)
export default class FiltersContainer extends Component {
  static propTypes = {
    audiences: PropTypes.array.isRequired,
    location: PropTypes.object,
    organizations: PropTypes.array.isRequired,
    push: PropTypes.func,
    queryParams: PropTypes.object,
    regions: PropTypes.array.isRequired,
    selectedOrganizations: PropTypes.array.isRequired,
    selectedAudiences: PropTypes.array.isRequired,
    selectedRegions: PropTypes.array.isRequired,
    selectedTags: PropTypes.array.isRequired,
    selectedTypes: PropTypes.array.isRequired,
    selectedVenues: PropTypes.array.isRequired,
    setSelectedAudiences: PropTypes.func.isRequired,
    setSelectedOrganizations: PropTypes.func.isRequired,
    setSelectedRegions: PropTypes.func.isRequired,
    setSelectedTags: PropTypes.func.isRequired,
    setSelectedTypes: PropTypes.func.isRequired,
    setSelectedVenues: PropTypes.func.isRequired,
    tags: PropTypes.array.isRequired,
    types: PropTypes.array.isRequired,
    venues: PropTypes.array.isRequired,
  };

  filtersAction(item) {
    const { push } = this.props; // eslint-disable-line no-shadow
    const selected = {
      audiences: this.props.selectedAudiences,
      organizations: this.props.selectedOrganizations,
      regions: this.props.selectedRegions,
      types: this.props.selectedTypes,
      tags: this.props.selectedTags,
      venues: this.props.selectedVenues,
    };
    const select = {
      audiences: this.props.setSelectedAudiences,
      organizations: this.props.setSelectedOrganizations,
      regions: this.props.setSelectedRegions,
      types: this.props.setSelectedTypes,
      tags: this.props.setSelectedTags,
      venues: this.props.setSelectedVenues,
    };

    const newQuery = updateSelections(item, selected, select);

    push({ pathname: '/evenements', query: newQuery });
  }

  filtersClear(types) {
    const { location, push } = this.props; // eslint-disable-line no-shadow
    const queryParams = { ...location.query };

    const select = {
      audiences: this.props.setSelectedAudiences,
      organizations: this.props.setSelectedOrganizations,
      regions: this.props.setSelectedRegions,
      types: this.props.setSelectedTypes,
      tags: this.props.setSelectedTags,
      venues: this.props.setSelectedVenues,
    };
    for (const type of types) {
      select[type]([]);
      delete queryParams[type];
    }

    push({ pathname: '/evenements', query: queryParams });
  }

  render() {
    return <Filters action={::this.filtersAction} clear={::this.filtersClear} {...this.props} />;
  }
}
