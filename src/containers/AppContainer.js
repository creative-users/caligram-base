import React, {Component, PropTypes} from 'react';
import Polyglot from 'node-polyglot';
import moment from 'moment';
import '../stylesheets/global.scss';
import { parseRange, splitIdsList } from 'helpers/utils';
import { App } from 'components';
import connectData from 'helpers/connectData';
import {
  areTopsLoaded as areAudiencesTopsLoaded,
  isLoaded as isAudiencesLoaded,
  isMenuProvided as isAudiencesMenuProvided,
  isMenuRequired as isAudiencesMenuRequired,
  load as loadAudiences,
  loadTops as loadTopsAudiences,
  setMenu as setAudiencesMenu,
  setSelected as setSelectedAudiences,
} from 'redux/modules/audiences';
import { selectRange } from 'redux/modules/events';
import {
  areTopsLoaded as areOrganizationsTopsLoaded,
  isLoaded as isOrganizationsLoaded,
  isMenuProvided as isOrganizationsMenuProvided,
  isMenuRequired as isOrganizationsMenuRequired,
  load as loadOrganizations,
  loadTops as loadTopsOrganizations,
  setMenu as setOrganizationsMenu,
  setSelected as setSelectedOrganizations,
} from 'redux/modules/organizations';
import {
  areTopsLoaded as areRegionsTopsLoaded,
  isLoaded as isRegionsLoaded,
  isMenuProvided as isRegionsMenuProvided,
  isMenuRequired as isRegionsMenuRequired,
  load as loadRegions,
  loadTops as loadTopsRegions,
  setMenu as setRegionsMenu,
  setSelected as setSelectedRegions,
} from 'redux/modules/regions';
import {
  areTopsLoaded as areTagsTopsLoaded,
  isLoaded as isTagsLoaded,
  isMenuProvided as isTagsMenuProvided,
  isMenuRequired as isTagsMenuRequired,
  load as loadTags,
  loadTops as loadTopsTags,
  setMenu as setTagsMenu,
  setSelected as setSelectedTags,
} from 'redux/modules/tags';
import {
  areTopsLoaded as areTypesTopsLoaded,
  isLoaded as isTypesLoaded,
  isMenuProvided as isTypesMenuProvided,
  isMenuRequired as isTypesMenuRequired,
  load as loadTypes,
  loadTops as loadTopsTypes,
  setMenu as setTypesMenu,
  setSelected as setSelectedTypes,
} from 'redux/modules/types';
import {
  areTopsLoaded as areVenuesTopsLoaded,
  isLoaded as isVenuesLoaded,
  isMenuProvided as isVenuesMenuProvided,
  isMenuRequired as isVenuesMenuRequired,
  load as loadVenues,
  loadTops as loadTopsVenues,
  setMenu as setVenuesMenu,
  setSelected as setSelectedVenues,
} from 'redux/modules/venues';

function fetchData(getState, dispatch, location) {
  const entityTypes = [
    {
      areTopsLoaded: areAudiencesTopsLoaded,
      isLoaded: isAudiencesLoaded,
      isMenuProvided: isAudiencesMenuProvided,
      isMenuRequired: isAudiencesMenuRequired,
      load: loadAudiences,
      loadTops: loadTopsAudiences,
      name: 'audiences',
      setMenu: setAudiencesMenu,
      setSelected: setSelectedAudiences,
    },
    {
      areTopsLoaded: areOrganizationsTopsLoaded,
      isLoaded: isOrganizationsLoaded,
      isMenuProvided: isOrganizationsMenuProvided,
      isMenuRequired: isOrganizationsMenuRequired,
      load: loadOrganizations,
      loadTops: loadTopsOrganizations,
      name: 'organizations',
      setMenu: setOrganizationsMenu,
      setSelected: setSelectedOrganizations,
    },
    {
      areTopsLoaded: areRegionsTopsLoaded,
      isLoaded: isRegionsLoaded,
      isMenuProvided: isRegionsMenuProvided,
      isMenuRequired: isRegionsMenuRequired,
      load: loadRegions,
      loadTops: loadTopsRegions,
      name: 'regions',
      setMenu: setRegionsMenu,
      setSelected: setSelectedRegions,
    },
    {
      areTopsLoaded: areTagsTopsLoaded,
      isLoaded: isTagsLoaded,
      isMenuProvided: isTagsMenuProvided,
      isMenuRequired: isTagsMenuRequired,
      load: loadTags,
      loadTops: loadTopsTags,
      name: 'tags',
      setMenu: setTagsMenu,
      setSelected: setSelectedTags,
    },
    {
      areTopsLoaded: areTypesTopsLoaded,
      isLoaded: isTypesLoaded,
      isMenuProvided: isTypesMenuProvided,
      isMenuRequired: isTypesMenuRequired,
      load: loadTypes,
      loadTops: loadTopsTypes,
      name: 'types',
      setMenu: setTypesMenu,
      setSelected: setSelectedTypes,
    },
    {
      areTopsLoaded: areVenuesTopsLoaded,
      isLoaded: isVenuesLoaded,
      isMenuProvided: isVenuesMenuProvided,
      isMenuRequired: isVenuesMenuRequired,
      load: loadVenues,
      loadTops: loadTopsVenues,
      name: 'venues',
      setMenu: setVenuesMenu,
      setSelected: setSelectedVenues,
    },
  ];

  const topsPromises = [];
  const missingMenuTypes = entityTypes.filter((type) => {
    return type.isMenuRequired() && !type.isMenuProvided();
  });
  missingMenuTypes.forEach((type) => {
    if (!type.areTopsLoaded(getState())) {
      topsPromises.push(dispatch(type.loadTops()));
    }
  });

  return Promise.all(topsPromises).then(() => {
    const loadPromises = [];
    entityTypes.forEach((type) => {
      if (!type.isLoaded(getState())) {
        const ids = splitIdsList(location.query[type.name])
          .concat(getState()[type.name].config.menu || [])
          .filter((id) => {
            return !getState()[type.name].data.find((item) => {
              return item.id === id;
            });
          });

        if (ids.length > 0) {
          loadPromises.push(
            dispatch(type.load(ids))
          );
        }
      }
    });

    return Promise.all(loadPromises).then(() => {
      if (!!location.query.range) {
        const range = parseRange(location.query.range);
        dispatch(selectRange(range));
      }

      entityTypes.forEach((type) => {
        if (type.isMenuRequired()) {
          if (type.isMenuProvided()) {
            dispatch(type.setMenu(getState()[type.name].config.menu));
          } else {
            dispatch(type.setMenu(getState()[type.name].tops.items.map((item) => { return item.id; })));
          }
        }

        dispatch(type.setSelected(location.query[type.name]));
      });
    });
  });
}

@connectData(fetchData)
export default class AppContainer extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired,
    params: PropTypes.object.isRequired,
    location: PropTypes.object,
  };

  static contextTypes = {
    store: PropTypes.object.isRequired,
  };

  static childContextTypes = {
    polyglot: PropTypes.object,
    moment: PropTypes.func,
  };

  state = {
    title: null,
  };

  getChildContext() {
    // TODO: get this info from redux router
    const locale = 'fr';
    const calendar = require('./../locales/fr/calendar.js');
    const phrases = require('./../locales/fr/home.js');

    Object.assign(
      phrases,
      require('./../locales/' + locale + '/event.js'),
    );

    // Setup momentjs
    moment.updateLocale(locale, calendar);

    return {
      polyglot: new Polyglot({
        locale: locale,
        phrases: phrases,
      }),
      moment: moment,
    };
  }

  componentWillMount() {
    this.logPageView();
  }

  componentWillReceiveProps() {
    this.logPageView();
  }

  setPageTitle(pageTitle) {
    this.setState({
      title: pageTitle,
    });
  }

  logPageView() {
    if (typeof window !== 'undefined' && !!window.ga) {
      window.ga('send', 'pageview');
    }
  }

  toggleSitesOverlay(event) {
    event.preventDefault();
  }

  render() {
    return (
      <div>
        <App children={this.props.children} setPageTitle={::this.setPageTitle} />
      </div>
    );
  }
}
