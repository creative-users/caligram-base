import React, { Component } from 'react';
import BodyClassName from 'react-body-classname';
import { index } from 'redux/modules/organizations';
import { EntitiesContainer } from '../containers';
import connectData from 'helpers/connectData';

function fetchData(getState, dispatch) {
  const state = getState().organizations;
  return dispatch(index(state.showAll, state.treeView));
}

@connectData(fetchData)
export default class OrganizationsContainer extends Component {
  render() {
    return (
      <BodyClassName className="organizations">
        <EntitiesContainer entity="organizations" />
      </BodyClassName>
    );
  }
}
