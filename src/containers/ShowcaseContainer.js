import React, { Component, PropTypes } from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { push } from 'redux-router';
import { EventList } from 'caligram-react';
import { stringify } from 'querystring';

@connect(state => ({
  showcases: state.showcase,
}), {push})
export default class ShowcaseContainer extends Component {
  static propTypes = {
    children: PropTypes.func,
    showcases: PropTypes.object,
    query: PropTypes.object.isRequired,
    push: PropTypes.func.isRequired,
    title: PropTypes.string,
  };

  static defaultProps = {
    children: (props) => {
      const dateFormat = ev => moment(ev.start_date).format('Do MMMM');
      return <EventList layout="carousel" groupBy="none" dateFormat={dateFormat} {...props} />;
    },
    title: 'En vedette',
  };

  render() {
    return (
      <div className="ShowcaseContainer">
        { this.props.children({
          title: this.props.title,
          events: this.props.showcases[stringify(this.props.query)].data,
        }) }
      </div>
    );
  }
}
