import React, { Component } from 'react';
import BodyClassName from 'react-body-classname';
import DocumentMeta from 'react-document-meta';
import { index as eventIndex, selectRange, setMapView, setRecurrences } from 'redux/modules/events';
import { Events } from '../components';
import connectData from 'helpers/connectData';
import moment from 'moment';
import { stringify } from 'querystring';

import config from '../config';

function fetchData(getState, dispatch, location) {
  const promises = [];

  const query = { ...location.query };
  if (query.range) {
    const range = query.range.split(',').map((time) => { return moment(time); });

    if (range.length) {
      const start = range[0] ? range[0].format('YYYY-MM-DD HH:mm:ss') : null;
      const end = range[1] ? range[1].format('YYYY-MM-DD HH:mm:ss') : null;
      promises.push(dispatch(selectRange({start: start, end: end})));
    }
  }

  if (query.recurrences) {
    promises.push(dispatch(setRecurrences(query.recurrences === 'true')));
  } else {
    query.recurrences = getState().events.recurrences;
  }

  if (query.mapView) {
    promises.push(dispatch(setMapView(query.mapView === 'true')));
  } else {
    query.mapView = getState().events.mapView;
  }

  query.order = 'start_date';

  // force reload events with back and next browser buttons
  if (query.mapView === 'false' || !query.mapView) {
    promises.push(dispatch(eventIndex('?' + stringify(query))));
  }

  return Promise.all(promises);
}

@connectData(fetchData)
export default class EventsContainer extends Component {
  render() {
    return (
      <BodyClassName className="events">
        <div>
          <DocumentMeta {...config.app} />
          <Events />
        </div>
      </BodyClassName>
    );
  }
}
