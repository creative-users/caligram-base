import React, { Component } from 'react';
import BodyClassName from 'react-body-classname';
import { index } from 'redux/modules/tags';
import { EntitiesContainer } from '../containers';
import connectData from 'helpers/connectData';

function fetchData(getState, dispatch) {
  const state = getState().types;
  return dispatch(index(state.showAll, state.treeView));
}

@connectData(fetchData)
export default class TagsContainer extends Component {
  render() {
    return (
      <BodyClassName className="tags">
        <EntitiesContainer entity="tags" />
      </BodyClassName>
    );
  }
}
