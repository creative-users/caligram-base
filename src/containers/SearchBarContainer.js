import React, { Component, PropTypes } from 'react';
import { SearchBar } from 'caligram-react';
import { connect } from 'react-redux';
import { autocomplete, clear } from 'redux/modules/search';
import { add as addAudience } from 'redux/modules/audiences';
import { add as addOrganization } from 'redux/modules/organizations';
import { add as addRegion } from 'redux/modules/regions';
import { add as addTag } from 'redux/modules/tags';
import { add as addType } from 'redux/modules/types';
import { add as addVenue } from 'redux/modules/venues';
import { push } from 'redux-router';

@connect(state => ({
  location: state.router.location,
  items: state.search.items,
}), dispatch => ({
  dispatchAddAudience: function(audience) {
    return dispatch(addAudience(audience));
  },
  dispatchAddRegion: function(audience) {
    return dispatch(addRegion(audience));
  },
  dispatchAddTag: function(tag) {
    return dispatch(addTag(tag));
  },
  dispatchAddType: function(type) {
    return dispatch(addType(type));
  },
  dispatchAddOrganization: function(org) {
    return dispatch(addOrganization(org));
  },
  dispatchAddVenue: function(venue) {
    return dispatch(addVenue(venue));
  },
  dispatchAutocomplete: function(query, searchIn, extra) {
    return dispatch(autocomplete(query, searchIn, extra));
  },
  dispatchClearSearch: function() {
    return dispatch(clear());
  },
  dispatchPush: function(obj) {
    return dispatch(push(obj));
  },
}))
export default class SearchBarContainer extends Component {
  static propTypes = {
    dispatchAddAudience: PropTypes.func.isRequired,
    dispatchAddOrganization: PropTypes.func.isRequired,
    dispatchAddTag: PropTypes.func.isRequired,
    dispatchAddType: PropTypes.func.isRequired,
    dispatchAddVenue: PropTypes.func.isRequired,
    dispatchAutocomplete: PropTypes.func.isRequired,
    dispatchClearSearch: PropTypes.func.isRequired,
    dispatchPush: PropTypes.func.isRequired,
    items: PropTypes.array,
    location: PropTypes.object,
  };

  static contextTypes = {
    history: PropTypes.object,
    location: PropTypes.object,
    polyglot: PropTypes.object,
  };

  constructor(props) {
    super(props);

    this.state = {
      value: props.location.query.q || '',
    };
  }

  onChange = (event, { newValue }) => {
    this.setState({
      value: newValue,
    });
  };

  onSubmit = (event) => {
    const { dispatchPush } = this.props; // eslint-disable-line no-shadow
    event.preventDefault();
    dispatchPush({ pathname: '/evenements', query: { q: this.state.value } });
  }

  onSuggestionsFetchRequested = ({ value }) => {
    const query = value.trim();

    const { dispatchAutocomplete } = this.props; // eslint-disable-line no-shadow

    if (this.state.timeout) {
      clearTimeout(this.state.timeout);
    }

    if (query.length >= 1) {
      this.setState({
        timeout: setTimeout(() => {
          dispatchAutocomplete(query);
        }, 250),
      }, );
    }
  };

  onSuggestionsClearRequested = () => {
    this.props.dispatchClearSearch();
  };

  onSuggestionSelected = (event, { suggestion }) => {
    event.preventDefault();
    const {
      dispatchAddAudience,
      dispatchAddOrganization,
      dispatchAddTag,
      dispatchAddType,
      dispatchAddVenue,
      dispatchPush,
    } = this.props; // eslint-disable-line no-shadow

    const query = {};
    query[suggestion.group] = suggestion.id;

    switch (suggestion.group) {
      case 'audiences':
        suggestion.tag = 'Public';
        dispatchAddAudience(suggestion);
        break;
      case 'organizations':
        suggestion.tag = 'Présenté par';
        dispatchAddOrganization(suggestion);
        break;
      case 'tags':
        suggestion.tag = 'Mot-clé';
        dispatchAddTag(suggestion);
        break;
      case 'types':
        suggestion.tag = 'Type';
        dispatchAddType(suggestion);
        break;
      case 'venues':
        suggestion.tag = 'Lieu';
        dispatchAddVenue(suggestion);
        break;
      case 'events':
        dispatchPush({ pathname: `/evenements/${suggestion.slug}/${suggestion.id}`});
        break;
      default:
        break;
    }

    if (suggestion.group !== 'events') {
      dispatchPush({ pathname: '/evenements', query: query });
    }

    this.setState({
      value: '',
    });
  };

  renderSectionTitle = (section) => {
    const {polyglot} = this.context;
    return polyglot.t(`event.entities.${section.title}`);
  };

  renderSuggestion = (suggestion) => {
    const {polyglot} = this.context;

    return (
      <div data-group={polyglot.t(`event.entities.${suggestion.group}`)}>
        {suggestion.image && <img src={suggestion.image} alt={suggestion.label}/>}
        <span>{suggestion.label}</span>
        {suggestion.venue && <div className="venue">{suggestion.venue}</div>}
      </div>
    );
  };

  render() {
    const properties = {
      onClear: this.onSuggestionsClearRequested,
      onFetch: this.onSuggestionsFetchRequested,
      onSelect: this.onSuggestionSelected,
      onSubmit: this.onSubmit,
      onChange: this.onChange,
      query: this.state.value,
      renderSectionTitle: this.renderSectionTitle,
      renderSuggestion: this.renderSuggestion,
      suggestions: this.props.items,
    };

    return <SearchBar {...properties} />;
  }
}
