import React, { Component, PropTypes } from 'react';
import BodyClassName from 'react-body-classname';
import DocumentMeta from 'react-document-meta';
import { index as eventIndex, setMapView, setRecurrences } from 'redux/modules/events';
import { index as showcaseIndex } from 'redux/modules/showcase';
import connectData from 'helpers/connectData';
import { Home } from 'components';

import config from '../config';

function fetchData(getState, dispatch, location) {
  const promises = [];

  const query = { ...location.query };
  if (query.recurrences) {
    promises.push(dispatch(setRecurrences(query.recurrences === 'true')));
  } else {
    query.recurrences = getState().events.recurrences;
  }

  if (query.mapView) {
    promises.push(dispatch(setMapView(query.mapView === 'true')));
  } else {
    query.mapView = getState().events.mapView;
  }

  query.order = 'start_date';

  // force reload events with back and next browser buttons
  if (query.mapView === 'false' || !query.mapView) {
    promises.push(dispatch(eventIndex(query)));

    if (config.app.showcases) {
      for (const showcase in config.app.showcases) {
        if (config.app.showcases.hasOwnProperty(showcase)) {
          promises.push(dispatch(showcaseIndex(config.app.showcases[showcase])));
        }
      }
    }
  }

  return Promise.all(promises);
}

@connectData(fetchData)
export default class HomeContainer extends Component {
  static propTypes = {
    setPageTitle: PropTypes.func.isRequired,
  };

  componentWillMount() {
    this.props.setPageTitle(config.app.title);
  }

  render() {
    return (
      <BodyClassName className="home">
        <div>
          <DocumentMeta {...config.app} />
          <Home />
        </div>
      </BodyClassName>
    );
  }
}
