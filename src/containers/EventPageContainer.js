import React, {Component, PropTypes} from 'react';
import BodyClassName from 'react-body-classname';
import { connect } from 'react-redux';
import { push } from 'redux-router';
import { EventPage, NotFound } from '../components';
import { isLoaded, show } from 'redux/modules/eventDetail';
import connectData from 'helpers/connectData';

function fetchData(getState, dispatch, location, params) {
  if (!isLoaded(getState())) {
    return dispatch(show(params.id));
  }
}

@connectData(fetchData)
@connect(state => ({
  event: state.eventDetail.data,
  error: state.eventDetail.error,
  location: state.router.location,
}), { push, show })
export default class EventPageContainer extends Component {
  static propTypes = {
    params: PropTypes.object.isRequired,
    error: PropTypes.any,
    event: PropTypes.object,
    location: PropTypes.any,
    push: PropTypes.func.isRequired,
    setPageTitle: PropTypes.func.isRequired,
  };

  componentWillMount() {
    if (this.props.event) {
      this.props.setPageTitle(this.props.event.title);
    }
  }

  render() {
    if (this.props.error) {
      switch (this.props.error.status_code) {
        case 404:
        default:
          return <NotFound />;
      }
    }

    return (
      <BodyClassName className="event">
        <EventPage {...this.props} />
      </BodyClassName>
    );
  }
}
