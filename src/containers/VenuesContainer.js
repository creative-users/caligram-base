import React, { Component } from 'react';
import BodyClassName from 'react-body-classname';
import { index } from 'redux/modules/venues';
import { EntitiesContainer } from '../containers';
import connectData from 'helpers/connectData';

function fetchData(getState, dispatch) {
  const state = getState().venues;
  return dispatch(index(state.showAll, state.treeView));
}

@connectData(fetchData)
export default class VenuesContainer extends Component {
  render() {
    return (
      <BodyClassName className="venues">
        <EntitiesContainer entity="venues" />
      </BodyClassName>
    );
  }
}
