import db from './db';
import defaults from 'lodash.defaults';
import values from 'lodash.values';
import uuid from 'uuid';

function Tokens() {
  function createToken(token) {
    defaults(token, {
      id: uuid.v1(),
      access_token: '',
      token_type: 'Bearer',
      expires_in: '3600',
      expired_at: new Date(),
    });

    db.set(token.id, token);

    return token;
  }

  function getToken(tokenId) {
    const token = db.get(tokenId);

    if (token) {
      return token;
    }
  }

  function getAllTokens() {
    return values(db.get());
  }

  function getLastToken() {
    return this.getAllTokens().pop();
  }

  function delAllTokens() {
    const tokens = this.getAllTokens();
    const that = this;

    tokens.map((token) => {
      that.delToken(token.id);
    });
  }

  function delToken(tokenId) {
    return db.delete(tokenId);
  }

  this.getToken = getToken;
  this.createToken = createToken;
  this.getAllTokens = getAllTokens;
  this.getLastToken = getLastToken;
  this.delAllTokens = delAllTokens;
  this.delToken = delToken;
  this.state = {};
}

export default new Tokens();
