import merge from 'lodash.merge';
import tokens from './tokens';
import OAuth from 'oauth';

const OAuth2 = OAuth.OAuth2;

function Caligram() {
  function serverAuthenticate(callback) {
    const oauth2 = new OAuth2(
      process.env.CALIGRAM_API_KEY,
      process.env.CALIGRAM_API_SECRET,
      process.env.CALIGRAM_API_URL,
      null,
      '/auth/authorize_client',
      null,
    );

    oauth2.getOAuthAccessToken(
      '',
      { grant_type: 'client_credentials' },
      (err, accessToken, refreshToken, results) => {
        if (err) {
          callback(err);
        } else {
          tokens.delAllTokens();

          const currentToken = tokens.createToken(merge(results, {
            expired_at: new Date(Date.now() + (results.expires_in * 1000)),
          }));

          callback(null, currentToken);
        }
      },
    );
  }

  this.serverAuthenticate = serverAuthenticate;
}

export default new Caligram();
