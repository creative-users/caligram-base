import qs from 'query-string';
import { partitionString } from 'prelude-extension';
import unidecode from 'unidecode';

const CLEAR = 'caligram/search/CLEAR';
const SEARCH = 'caligram/search/SEARCH';
const SEARCH_SUCCESS = 'caligram/search/SEARCH_SUCCESS';
const SEARCH_FAIL = 'caligram/search/SEARCH_FAIL';

const initialState = {
  loaded: false,
  loading: false,
  promise: null,
  items: [],
};

export default function search(state = initialState, action = {}) {
  function parseResults(result, query) {
    const suggestions = [];

    Object.keys(result).map((section) => {
      suggestions.push({
        title: section,
        data: result[section].data.map((item) => {
          return {
            id: item.id,
            slug: item.slug,
            label: item.name,
            group: section,
            image: item.image,
            venue: item.venue,
            labelPartitions: partitionString(
              unidecode(item.name).toLowerCase(),
              unidecode(query).toLowerCase()
            ),
          };
        }),
      });
    });

    return suggestions;
  }

  switch (action.type) {
    case CLEAR:
      return {
        ...state,
        items: [],
      };
    case SEARCH:
      if (state.promise && state.promise.cancel) {
        state.promise.cancel();
      }
      return {
        ...state,
        loaded: false,
        loading: true,
        promise: action.promise,
      };
    case SEARCH_SUCCESS:
      return {
        ...state,
        loaded: true,
        loading: false,
        items: parseResults(action.result, action.query),
        promise: null,
      };
    case SEARCH_FAIL:
      return {
        ...state,
        loaded: false,
        loading: false,
        error: action.error,
        promise: null,
      };
    default:
      return state;
  }
}

export function autocomplete(input, searchIn, extra) {
  let params = { q: input };
  if (searchIn && searchIn.length) {
    params = {
      ...params,
      ...extra,
      searchIn: typeof searchIn === 'object' ? searchIn.join(',') : searchIn,
    };
  }

  return {
    types: [SEARCH, SEARCH_SUCCESS, SEARCH_FAIL],
    query: input,
    promise: client => { return client.get(`/search?${qs.stringify(params)}`); },
  };
}

export function clear() {
  return {
    type: CLEAR,
  };
}

export function isLoaded(globalState) {
  return globalState.search && globalState.search.loaded;
}

export function isLoading(globalState) {
  return globalState.search && globalState.search.loading;
}
