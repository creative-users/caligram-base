import {
  filterCollectionFromParams,
  findInCollectionOrAdd,
  mergeCollections,
  orderByField,
  splitIdsList,
} from 'helpers/utils';
import { stringify } from 'querystring';
import { app } from 'config';

export default function BaseReducer(entity, name) {
  const ADD = entity + '/ADD';
  const LOAD = entity + '/LOAD';
  const LOAD_FAIL = entity + '/LOAD_FAIL';
  const LOAD_SUCCESS = entity + '/LOAD_SUCCESS';
  const LOAD_TOPS = entity + '/LOAD_TOPS';
  const LOAD_TOPS_FAIL = entity + '/LOAD_TOPS_FAIL';
  const LOAD_TOPS_SUCCESS = entity + '/LOAD_TOPS_SUCCESS';
  const SET_SELECTED = entity + '/SET_SELECTED';
  const SET_MENU = entity + '/SET_MENU';
  const TOGGLE_DISCLOSED = entity + '/TOGGLE_DISCLOSED';
  const SET_SHOW_ALL = entity + '/SET_SHOW_ALL';
  const SET_TREE_VIEW = entity + '/SET_TREE_VIEW';

  const config = app.entities[entity];
  const initialData = (config.initialData || []);
  const initialState = {
    data: initialData,
    config,
    loaded: false,
    loading: false,
    menu: [],
    selected: [],
    disclosed: [],
    showAll: config.showAll !== undefined ? config.showAll : false,
    treeView: config.treeView !== undefined ? config.treeView : false,
    tops: {
      loaded: false,
      loading: false,
      items: [],
    },
  };

  function toggleItem(sourceList, item) {
    const index = sourceList.indexOf(item);
    switch (index > -1) {
      case true: // Present
        sourceList.splice(index, 1);
        break;
      case false: // Absent
      default:
        sourceList.push(item);
    }
    return sourceList;
  }

  function mergeEntities(toMerge, target) {
    const parsedData = toMerge.map((ent) => {
      return {
        group: entity,
        id: ent.id,
        label: ent.name,
        name: ent.name,
        parent_id: ent.parent_id,
        references: ent.references,
        tag: name,
      };
    });

    return mergeCollections(parsedData, target);
  }

  return {
    reducer(state = initialState, action = {}) {
      switch (action.type) {
        case ADD:
          return {
            ...state,
            data: findInCollectionOrAdd(action.item, state.data),
          };
        case LOAD:
          return {
            ...state,
            loaded: false,
            loading: true,
          };
        case LOAD_FAIL:
          return {
            ...state,
            loaded: false,
            loading: false,
          };
        case LOAD_SUCCESS:
          let data;
          // This exists only because all LOAD, LOAD_TOPS and INDEX merge
          // results between executions, which is useful to keep an history
          // of existing values for filters, but not for lists.
          // We should eventually refactor this to always keep a merged list
          // of seen items, and a separate collection of active items.
          let newData = [];
          if (Array.isArray(action.result)) {
            action.result.forEach((aData) => {
              newData = newData.concat(aData.data);
            });
          } else {
            newData = action.result.data;
          }

          if (action.flush) {
            data = newData;
          } else {
            data = newData
              ? mergeEntities(newData, state.data)
              : newData;
          }

          return {
            ...state,
            data,
            loaded: true,
            loading: false,
          };
        case LOAD_TOPS:
          const newLoadTops = {
            items: [],
            loaded: false,
            loading: true,
          };
          return {
            ...state,
            tops: newLoadTops,
          };
        case LOAD_TOPS_FAIL:
          const newLoadTopsFail = {
            items: [],
            loaded: false,
            loading: false,
          };
          return {
            ...state,
            tops: newLoadTopsFail,
          };
        case LOAD_TOPS_SUCCESS:
          const newLoadTopsSuccess = {
            items: action.result.data ? mergeEntities(action.result.data, state.tops.items) : state.tops.items,
            loaded: true,
            loading: false,
          };
          return {
            ...state,
            data: action.result.data ? mergeEntities(action.result.data, state.data) : state.data,
            loaded: true,
            loading: false,
            tops: newLoadTopsSuccess,
          };
        case TOGGLE_DISCLOSED:
          return {
            ...state,
            disclosed: toggleItem(state.disclosed, action.value),
          };
        case SET_MENU:
          const filteredCollection = filterCollectionFromParams(state.data, action.menu);

          return {
            ...state,
            menu: config.menuOrder ? orderByField(
              filteredCollection,
              config.menuOrder
            ) : filteredCollection,
          };
        case SET_SELECTED:
          return {
            ...state,
            selected: filterCollectionFromParams(state.data, action.selected, entity),
          };
        case SET_SHOW_ALL:
          return {
            ...state,
            showAll: action.value,
          };
        case SET_TREE_VIEW:
          return {
            ...state,
            treeView: action.value,
          };
        default:
          return state;
      }
    },

    add(object) {
      return {
        type: ADD,
        item: object,
      };
    },

    areTopsLoaded(globalState) {
      return globalState[entity] && globalState[entity].tops && globalState[entity].tops.loaded;
    },

    index(showAll, treeView, query = {}, flush = false) {
      const querystring = stringify(
        Object.assign({}, {
          direction: (showAll ? 'asc' : 'desc'),
          order: (showAll ? 'name' : 'references'),
          per: 1000,
        }, query)
      );

      let promise;
      if (!showAll) {
        if (treeView) {
          promise = (client) => {
            const fetchParents = (res) => {
              const process = new Promise(function(resolve) {
                const fetchedIds = [];
                const parentIds = [];

                res.data.forEach((eId) => {
                  fetchedIds.push(eId.id);
                  if (eId.parent_id) {
                    parentIds.push(eId.parent_id);
                  }
                });

                const unfetchedIds = parentIds.filter((pId) => {
                  return fetchedIds.indexOf(pId) === -1;
                });

                if (unfetchedIds.length > 0) {
                  client.get(`/${entity}?id=${unfetchedIds}`)
                    .then((results) => {
                      fetchParents({ data: results.data.concat(res.data) }).then(resolve);
                    });
                } else {
                  resolve(res);
                }
              });

              return process;
            };

            return client.get(
              (process.env.CALENDAR_ID ? `calendars/${process.env.CALENDAR_ID}/` : '') +
                `${entity}?${querystring}`
            ).then(fetchParents);
          };
        } else {
          promise = (client) => {
            return client.get(
              (process.env.CALENDAR_ID ? `calendars/${process.env.CALENDAR_ID}/` : '') +
                `${entity}?${querystring}`
            );
          };
        }
      } else {
        promise = (client) => {
          return client.get(`/${entity}?${querystring}`);
        };
      }

      return {
        types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
        flush,
        promise,
      };
    },

    isLoaded(globalState) {
      return globalState[entity] && globalState[entity].loaded;
    },

    isMenuProvided() {
      return Array.isArray(config.menu) && config.menu.length > 0;
    },

    isMenuRequired() {
      return config.inFilters;
    },

    load(ids) {
      if (!Array.isArray(ids)) {
        return { type: LOAD_FAIL };
      }

      return {
        types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
        promise: (client) => {
          return client.get(
            (process.env.CALENDAR_ID ? `calendars/${process.env.CALENDAR_ID}/` : '') +
              `${entity}?id=${ids.join(',')}`
          );
        },
      };
    },

    loadTops() {
      return {
        types: [LOAD_TOPS, LOAD_TOPS_SUCCESS, LOAD_TOPS_FAIL],
        promise: (client) => {
          return client.get(
            (process.env.CALENDAR_ID ? `calendars/${process.env.CALENDAR_ID}/` : '') +
              `${entity}?order=references&direction=desc&per=${config.limit || 10}`
          );
        },
      };
    },

    toggleDisclosed(value) {
      return {
        type: TOGGLE_DISCLOSED,
        value,
      };
    },

    setMenu(menu) {
      return {
        type: SET_MENU,
        menu,
      };
    },

    setSelected(selected) {
      return {
        type: SET_SELECTED,
        selected: Array.isArray(selected) ? selected : splitIdsList(selected),
      };
    },

    setShowAll(value) {
      return {
        type: SET_SHOW_ALL,
        value,
      };
    },

    setTreeView(value) {
      return {
        type: SET_TREE_VIEW,
        value,
      };
    },
  };
}
