import { combineReducers } from 'redux';
import { routerStateReducer } from 'redux-router';

import { reducer as audiences } from './audiences';
import events from './events';
import eventDetail from './eventDetail';
import showcase from './showcase';
import { reducer as organizations } from './organizations';
import { reducer as regions } from './regions';
import { reducer as tags } from './tags';
import { reducer as types } from './types';
import search from './search';
import { reducer as venues } from './venues';

export default combineReducers({
  router: routerStateReducer,
  audiences,
  events,
  eventDetail,
  showcase,
  organizations,
  regions,
  tags,
  types,
  search,
  venues,
});
