import { stringify } from 'querystring';

const LOAD = 'showcase/LOAD';
const LOAD_SUCCESS = 'showcase/LOAD_SUCCESS';
const LOAD_FAIL = 'showcase/LOAD_FAIL';

export default function showcase(state = {}, action = {}) {
  switch (action.type) {
    case LOAD:
      const collection = state[action.slug];
      if (collection && collection.promise && collection.promise.cancel) {
        collection.promise.cancel();
      }
      return {
        ...state,
        [action.slug]: {
          loaded: false,
          loading: true,
          promise: action.promise,
        },
      };
    case LOAD_FAIL:
      return {
        ...state,
        [action.slug]: {
          error: action.error,
          loaded: false,
          promise: null,
        },
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        [action.slug]: {
          data: action.result.data ? action.result.data : action.result,
          loaded: true,
          loading: false,
          pagination: action.result.meta.pagination ? action.result.meta.pagination : null,
          promise: null,
        },
      };
    default:
      return state;
  }
}

export function index(query) {
  const defaultOptions = {
    order: 'start_date',
    per: 10,
    recurrences: false,
  };
  const options = { ...defaultOptions, ...query };

  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    slug: stringify(query),
    promise: (client) => {
      return client.get(
        (process.env.CALENDAR_ID ? `calendars/${process.env.CALENDAR_ID}/` : '') +
          `events?${stringify(options)}`
      );
    },
  };
}
