import React from 'react';
import { IndexRoute, Route } from 'react-router';
import { About, NotFound } from 'components';
import {
  AppContainer,
  EventPageContainer,
  EventsContainer,
  HomeContainer,
  OrganizationsContainer,
  TagsContainer,
  TypesContainer,
  VenuesContainer,
} from 'containers';

export default () => {
  const locale = 'fr';
  const routes = [
    {
      paths: { fr: ['/evenements'], en: ['/events'] },
      component: EventsContainer,
    },
    {
      paths: { fr: ['/evenements/:slug/:id'], en: ['/events/:slug/:id'] },
      component: EventPageContainer,
    },
    {
      paths: { fr: ['/a-propos'], en: ['/about'] },
      component: About,
    },
    {
      paths: { fr: ['/types'], en: ['/types'] },
      component: TypesContainer,
    },
    {
      paths: { fr: ['/themes'], en: ['/themes'] },
      component: TagsContainer,
    },
    {
      paths: { fr: ['/lieux'], en: ['/venues'] },
      component: VenuesContainer,
    },
    {
      paths: { fr: ['/groupes'], en: ['/groups'] },
      component: OrganizationsContainer,
    },
  ];

  return (
    <Route path="/" component={AppContainer}>
      { /* Home (main) route */ }
      <IndexRoute component={HomeContainer} />

      { /* Routes */ }
      {routes.map((route, index) => (
        <Route key={index} component={route.component}>
          {route.paths[locale].map((path, ind) => (
            <Route key={ind} path={path} />
              ))}
        </Route>
        ))}

      { /* Catch all route */ }
      <Route path="*" component={NotFound} status={404} />
    </Route>
  );
};
